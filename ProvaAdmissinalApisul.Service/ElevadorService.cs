﻿using Newtonsoft.Json;
using ProvaAdmissinalApisul.Domain;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ProvaAdmissinalApisul.Service
{
    public class ElevadorService : IElevadorService
    {
        #region ---- privados ----
        private List<AdministracaoPreditoDominio> BaseDeDados()
        {
            var dInfo = new DirectoryInfo(Environment.CurrentDirectory);
            var file = dInfo.GetFiles("input.json").FirstOrDefault();
            var fileText = File.ReadAllText(file.Name);
            return JsonConvert.DeserializeObject<List<AdministracaoPreditoDominio>>(fileText);
        }

        private float CalculoPercentualElevador(char elevador)
        {
            var resultado = (from T in BaseDeDados()
                             group T by T.Elevador into G
                             orderby G.Count()
                             select new { Elevador = G.Key, Total = G.Count() });

            var totalValorElevador = resultado.Where(x => x.Elevador == elevador).Sum(x => x.Total);

            var total = resultado.Sum(x => x.Total);

            var percent = (double.Parse(totalValorElevador.ToString()) / total) * 100;

            return float.Parse(Math.Round(percent,2).ToString());
        }
        #endregion

        public List<int> AndarMenosUtilizado()
        {
            var resultado = (from T in BaseDeDados()
                             group T by T.Andar into G
                             orderby G.Count()
                             select new { Andar = G.Key, Total = G.Count() });

            var andarMenosUtilizado = resultado.FirstOrDefault();

            return resultado.Where(x => x.Total == andarMenosUtilizado.Total).Select(x => x.Andar).OrderBy(x => x).ToList();
        }

        public List<char> ElevadorMaisFrequentado()
        {
            var resultado = (from T in BaseDeDados()
                             group T by T.Elevador into G
                             orderby G.Count() descending
                             select new { Elevador = G.Key, Total = G.Count() });

            var elevadorMaisFrequentado = resultado.FirstOrDefault();

            return resultado.Where(x => x.Total == elevadorMaisFrequentado.Total).Select(x => x.Elevador).OrderBy(x => x).ToList();
        }

        public List<char> ElevadorMenosFrequentado()
        {
            var resultado = (from T in BaseDeDados()
                             group T by T.Elevador into G
                             orderby G.Count()
                             select new { Elevador = G.Key, Total = G.Count() });

            var elevadorMenosFrequentado = resultado.FirstOrDefault();

            return resultado.Where(x => x.Total == elevadorMenosFrequentado.Total).Select(x => x.Elevador).ToList();
        }

        public float PercentualDeUsoElevadorA() => CalculoPercentualElevador('A');

        public float PercentualDeUsoElevadorB() => CalculoPercentualElevador('B');

        public float PercentualDeUsoElevadorC() => CalculoPercentualElevador('C');

        public float PercentualDeUsoElevadorD() => CalculoPercentualElevador('D');

        public float PercentualDeUsoElevadorE() => CalculoPercentualElevador('E');

        public List<char> PeriodoMaiorFluxoElevadorMaisFrequentado()
        { 
            var resultado = (from T in BaseDeDados()
                             group T by new { T.Elevador , T.Turno} into G
                             orderby G.Count() descending
                             select new { G.Key.Elevador, G.Key.Turno, Total = G.Count() });

            var periodoMaiorFluxoElevadorMaisFrequentado = resultado.FirstOrDefault();

            return resultado.Where(x => x.Total == periodoMaiorFluxoElevadorMaisFrequentado.Total).Select(x => x.Turno).Distinct().OrderBy(x => x).ToList();
        }

        public List<char> PeriodoMaiorUtilizacaoConjuntoElevadores()
        {
            var resultado = (from T in BaseDeDados()
                             group T by T.Turno into G
                             orderby G.Count() descending
                             select new { Turno = G.Key, Total = G.Count() });

            var periodoMaiorUtilizacaoConjuntoElevadores = resultado.FirstOrDefault();

            return resultado.Where(x => x.Total == periodoMaiorUtilizacaoConjuntoElevadores.Total).Select(x => x.Turno).OrderBy(x => x).ToList();
        }

        public List<char> PeriodoMenorFluxoElevadorMenosFrequentado()
        {
            var resultado = (from T in BaseDeDados()
                             group T by new { T.Elevador, T.Turno } into G
                             orderby G.Count() 
                             select new { G.Key.Elevador, G.Key.Turno, Total = G.Count() });

            var periodoMaiorFluxoElevadorMaisFrequentado = resultado.FirstOrDefault();

            return resultado.Where(x => x.Total == periodoMaiorFluxoElevadorMaisFrequentado.Total).Select(x => x.Turno).Distinct().OrderBy(x => x).ToList();
        }
    }
}
