using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProvaAdmissinalApisul.Service;
using System;
using System.Linq;

namespace ProvaAdmissinalApisul.Test
{
    [TestClass]
    public class ResultadoProvaTest
    {
        [TestMethod]
        public void AndarMenosUtilizado()
        {
            var resultado = new ElevadorService().AndarMenosUtilizado();
            if (resultado.Any())
            {
                Console.WriteLine("AndarMenosUtilizado");
                resultado.ForEach(x => Console.WriteLine(x));
                Assert.IsNotNull(resultado);
            }
        }

        [TestMethod]
        public void ElevadorMaisFrequentado()
        {
            var resultado = new ElevadorService().ElevadorMaisFrequentado();
            if (resultado.Any())
            {
                Console.WriteLine("ElevadorMaisFrequentado");
                resultado.ForEach(x => Console.WriteLine(x));
                Assert.IsNotNull(resultado);
            }
        }

        [TestMethod]
        public void ElevadorMenosFrequentado()
        {
            var resultado = new ElevadorService().ElevadorMenosFrequentado();
            if (resultado.Any())
            {
                Console.WriteLine("ElevadorMenosFrequentado");
                resultado.ForEach(x => Console.WriteLine(x));
                Assert.IsNotNull(resultado);
            }
        }

        [TestMethod]
        public void PercentualDeUsoElevadorA()
        {
            var resultado = new ElevadorService().PercentualDeUsoElevadorA();
            Console.WriteLine("PercentualDeUsoElevadorA");
            Console.WriteLine(resultado);
            Assert.IsNotNull(resultado);
        }

        [TestMethod]
        public void PercentualDeUsoElevadorB()
        {
            var resultado = new ElevadorService().PercentualDeUsoElevadorB();
            Console.WriteLine("PercentualDeUsoElevadorB");
            Console.WriteLine(resultado);
            Assert.IsNotNull(resultado);
        }

        [TestMethod]
        public void PercentualDeUsoElevadorC()
        {
            var resultado = new ElevadorService().PercentualDeUsoElevadorC();
            Console.WriteLine("PercentualDeUsoElevadorC");
            Console.WriteLine(resultado);
            Assert.IsNotNull(resultado);
        }

        [TestMethod]
        public void PercentualDeUsoElevadorD()
        {
            var resultado = new ElevadorService().PercentualDeUsoElevadorD();
            Console.WriteLine("PercentualDeUsoElevadorD");
            Console.WriteLine(resultado);
            Assert.IsNotNull(resultado);
        }

        [TestMethod]
        public void PercentualDeUsoElevadorE()
        {
            var resultado = new ElevadorService().PercentualDeUsoElevadorE();
            Console.WriteLine("PercentualDeUsoElevadorE");
            Console.WriteLine(resultado);
            Assert.IsNotNull(resultado);
        }

        [TestMethod]
        public void PeriodoMaiorFluxoElevadorMaisFrequentado()
        {
            var resultado = new ElevadorService().PeriodoMaiorFluxoElevadorMaisFrequentado();
            Console.WriteLine("PeriodoMaiorFluxoElevadorMaisFrequentado");
            resultado.ForEach(x => Console.WriteLine(x));
            Assert.IsNotNull(resultado);
        }

        [TestMethod]
        public void PeriodoMenorFluxoElevadorMenosFrequentado()
        {
            var resultado = new ElevadorService().PeriodoMenorFluxoElevadorMenosFrequentado();
            Console.WriteLine("PeriodoMenorFluxoElevadorMenosFrequentado");
            resultado.ForEach(x => Console.WriteLine(x));
            Assert.IsNotNull(resultado);
        }

        [TestMethod]
        public void PeriodoMaiorUtilizacaoConjuntoElevadores()
        {
            var resultado = new ElevadorService().PeriodoMaiorUtilizacaoConjuntoElevadores();
            Console.WriteLine("PeriodoMaiorUtilizacaoConjuntoElevadores");
            resultado.ForEach(x => Console.WriteLine(x));
            Assert.IsNotNull(resultado);
        }
    }
}
