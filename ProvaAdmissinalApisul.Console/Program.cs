﻿using ProvaAdmissinalApisul.Service;
using System;
using System.Linq;

namespace ProvaAdmissinalApisul
{
    class Program
    {
        static void Main(string[] args)
        {
            AndarMenosUtilizado();
            ElevadorMaisFrequentado();
            ElevadorMaisFrequentado();
            ElevadorMenosFrequentado();
            PercentualDeUsoElevadorA();
            PercentualDeUsoElevadorB();
            PercentualDeUsoElevadorC();
            PercentualDeUsoElevadorD();
            PercentualDeUsoElevadorE();
            PeriodoMaiorFluxoElevadorMaisFrequentado();
            PeriodoMenorFluxoElevadorMenosFrequentado();
            PeriodoMaiorUtilizacaoConjuntoElevadores();
        }
       
        public static void AndarMenosUtilizado()
        {
            var resultado = new ElevadorService().AndarMenosUtilizado();
            if (resultado.Any())
            {
                Console.WriteLine("AndarMenosUtilizado");
                resultado.ForEach(x => Console.WriteLine(x));
            }
        }
               
        public static void ElevadorMaisFrequentado()
        {
            var resultado = new ElevadorService().ElevadorMaisFrequentado();
            if (resultado.Any())
            {
                Console.WriteLine("ElevadorMaisFrequentado");
                resultado.ForEach(x => Console.WriteLine(x));
            }
        }
       
        public static void ElevadorMenosFrequentado()
        {
            var resultado = new ElevadorService().ElevadorMenosFrequentado();
            if (resultado.Any())
            {
                Console.WriteLine("ElevadorMenosFrequentado");
                resultado.ForEach(x => Console.WriteLine(x));
            }
        }
               
        public static void PercentualDeUsoElevadorA()
        {
            var resultado = new ElevadorService().PercentualDeUsoElevadorA();
            Console.WriteLine("PercentualDeUsoElevadorA");
            Console.WriteLine(resultado);
        }
       
        public static void PercentualDeUsoElevadorB()
        {
            var resultado = new ElevadorService().PercentualDeUsoElevadorB();
            Console.WriteLine("PercentualDeUsoElevadorB");
            Console.WriteLine(resultado);
        }
       
        public static void PercentualDeUsoElevadorC()
        {
            var resultado = new ElevadorService().PercentualDeUsoElevadorC();
            Console.WriteLine("PercentualDeUsoElevadorC");
            Console.WriteLine(resultado);
        }
       
        public static void PercentualDeUsoElevadorD()
        {
            var resultado = new ElevadorService().PercentualDeUsoElevadorD();
            Console.WriteLine("PercentualDeUsoElevadorD");
            Console.WriteLine(resultado);
        }
       
        public static void PercentualDeUsoElevadorE()
        {
            var resultado = new ElevadorService().PercentualDeUsoElevadorE();
            Console.WriteLine("PercentualDeUsoElevadorE");
            Console.WriteLine(resultado);
        }
       
        public static void PeriodoMaiorFluxoElevadorMaisFrequentado()
        {
            var resultado = new ElevadorService().PeriodoMaiorFluxoElevadorMaisFrequentado();
            Console.WriteLine("PeriodoMaiorFluxoElevadorMaisFrequentado");
            resultado.ForEach(x => Console.WriteLine(x));
        }
               
        public static void PeriodoMenorFluxoElevadorMenosFrequentado()
        {
            var resultado = new ElevadorService().PeriodoMenorFluxoElevadorMenosFrequentado();
            Console.WriteLine("PeriodoMenorFluxoElevadorMenosFrequentado");
            resultado.ForEach(x => Console.WriteLine(x));
        }
       
        public static void PeriodoMaiorUtilizacaoConjuntoElevadores()
        {
            var resultado = new ElevadorService().PeriodoMaiorUtilizacaoConjuntoElevadores();
            Console.WriteLine("PeriodoMaiorUtilizacaoConjuntoElevadores");
            resultado.ForEach(x => Console.WriteLine(x));
        }
    }
}
