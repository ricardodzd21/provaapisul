﻿using System;

namespace ProvaAdmissinalApisul.Domain
{
    public class AdministracaoPreditoDominio
    {
        public int Andar { get; set; }
        public char Elevador { get; set; }
        public char Turno { get; set; }
    }
}
